[id='{module}_config_user']
User:: Use this directive to set the user ownership for the created
  socket or pipe or file. By default, this is the user {productName} is running as (which may
  be specified by the global <<config_global_user,User>> directive).
  This directive is not currently supported on Windows.

